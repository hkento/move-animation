//htmlに、実際に固定する要素に"target"クラス、それを覆うdivに"target-wrapper"クラスをつけることで、
//スクロールして画面上の指定位置に達したときに要素をfixedし、
//スクロールに合わせて以降の"target"クラス要素の位置まで移動します。
$(function() {
  var fixHeight = $(window).height() * (1 / 2); //固定する高さ

  var $allTarget = $(".target");
  var $allTargetWrapper = $(".target-wrapper");
  var initialLefts = []; //すべてのtargetのleftの初期値 移動するとズレてしまうため取っておく
  $allTarget.each(function() {
    initialLefts.push(castStrPxToNum($(this).css("left")));
  });
  var $firstTarget = $($allTarget[0]);
  var $firstTargetWrapper = $firstTarget.parent();
  var $finalTarget = $($allTarget[$allTarget.length - 1]);

  $(window).scroll(function() {

    $allTargetWrapper.each(function() {
      toggleFixReachedTo(this, fixHeight);
    });

    if ($firstTarget.hasClass("fixed") && ! $finalTarget.hasClass("fixed")) { moveFixedTargets($allTarget); } //最初がfixされていて最後のtargetがfixされていない場合、fixされている要素を動かします

    if (! $firstTarget.hasClass("fixed")) { $firstTarget.css("left", initialLefts[0]); } //最初のtargetがfixされていなかったら、leftを最初の位置に戻します(計算だけだと微妙にズレるため。毎回違うズレ方をするため、計算式ではなく処理速度の問題と思われる)
    if ($finalTarget.hasClass("fixed")) { //最後の要素がfixされたら、すべてのleftを最後の要素に合わせます。同上。
      $allTarget.each(function () {
        $(this).css("left", initialLefts[initialLefts.length - 1]);
      });
    }
  });

  // $allTargetの配列のソートのされ方に依存します
  // HTML上の上部にある要素からソートされている想定です
  function moveFixedTargets($allTarget) {
    var currentIndex; //最後に固定されたtargetのインデックス
    var nextIndex; //次に固定されるtargetのインデックス
    $allTarget.each(function (i) {
      if (! $(this).hasClass("fixed")) {
        nextIndex = i;
        currentIndex = i - 1;
        return false; //break
      }
    });

    var currentTargetWrapperYPos = getAbsoluteYPosition($($allTarget[currentIndex]).parent()[0]);
    var nextTargetWrapperYPos = getAbsoluteYPosition($($allTarget[nextIndex]).parent()[0]);
    var movePos = calcMoveLeftPosition(initialLefts[currentIndex], initialLefts[nextIndex], currentTargetWrapperYPos, nextTargetWrapperYPos);
    for (var i = 0; i <= currentIndex; i++) {
      move($allTarget[i], movePos); //fixされた要素を動かします
    }
  }

  // どこから(fromY)どこまで(toY)という縦座標と最終的に移動したいleftから、
  // 今のスクロール位置で適応すべきleftを算出します
  function calcMoveLeftPosition(leftFrom, leftTo, fromY, toY) {
    var wholeYLen = toY - fromY;
    var leftDiff = leftTo - leftFrom;
    var scrollRatio = detectScrollAmount(fromY) / wholeYLen;
    var moveDiff = leftDiff * scrollRatio;
    return leftFrom + moveDiff;
  }

  function detectScrollAmount(from = 0) {
    return window.pageYOffset - from;
  }

  function move(target, to) {
    $(target).css("left", to + "px");
  }

  //fixHeight(px)のウインドウの高さに到達したtargetWrapperのtargetをfixし、
  //戻ってfixHeightから離れたらfixをはずします
  function toggleFixReachedTo(targetWrapper, fixHeight) {
    var $target = $(targetWrapper).find(".target");
    if (isReachedTo(targetWrapper, fixHeight)) {
      $target.addClass("fixed");
      $target.css("bottom", fixHeight);
    } else {
      $target.removeClass("fixed");
      $target.css("bottom", "");
    }
  }

  //targetWrapperが画面のfixHeight(px)の高さに届いたか判定します
  function isReachedTo(targetWrapper, fixHeight) {
    var targetWrapperAbslYPos = getAbsoluteYPosition(targetWrapper);
    var scrollPos = window.pageYOffset;
    var windowHeight = $(window).height();
    var targetHeight = castStrPxToNum($(targetWrapper).find(".target").css("height"));

    return scrollPos > (targetWrapperAbslYPos - windowHeight + fixHeight + targetHeight);
  }

  function getAbsoluteYPosition(target) {
    var rect = target.getBoundingClientRect();
    return rect.top + window.pageYOffset;
  }

  //"100px"のような文字列を100のような数値に変換します
  function castStrPxToNum(strPx) {
    return Number(strPx.substr(0, strPx.length -2));
  }

});
