# move-animation #

画面上の特定位置に`target`クラスをつけた要素が来たらその場に`fixed`し、次の`target`に向かってスクロールに合わせて水平方向に移動します。

## Usage ##
実際にfixed、アニメーションしたい画像や要素に`target`クラスをつけ、それを`target-wrapper`クラスのついた`div`で囲んでください。

一番最初(HTMLの表示上で一番上にくる、fixされる要素)については、`target`クラスの他に`first-target`クラスをつけてください。